import { PathParser, PathSyntaxError, InvalidIndexError } from '../src/path-parser';
import { Placeholders, Names } from '../src/placeholders';

test('path-syntax-error', () => {
    expect(() => PathParser.parse('')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse('.')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse('\\')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse('[')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse(']')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse('a[')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse('a[0')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse('a[b')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse('a[]')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse('a..b')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse('a[0]bc')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse('a[0].')).toThrow(PathSyntaxError);
    expect(() => PathParser.parse('a[0][')).toThrow(PathSyntaxError);
});

test('invalid-index-error', () => {
    expect(() => PathParser.parse('a[00]')).toThrow(InvalidIndexError);
});

test('default', () => {
    const names: Names = new Placeholders('#');
    const expression: string = PathParser.join(PathParser.parse('monster.space.network'), names);

    expect(expression).toBe('#0.#1.#2');
    expect(Object.fromEntries(names)).toEqual({
        '#0': 'monster',
        '#1': 'space',
        '#2': 'network',
    });
});

test('escape', () => {
    const names: Names = new Placeholders('#');
    const expression: string = PathParser.join(PathParser.parse('\\\\\\.\\[\\]'), names);

    expect(expression).toBe('#0');
    expect(Object.fromEntries(names)).toEqual({
        '#0': '\\.[]',
    });
});

test('index', () => {
    const names: Names = new Placeholders('#');
    const expression: string = PathParser.join(PathParser.parse('a[0].b[1][2][3]'), names);

    expect(expression).toBe('#0[0].#1[1][2][3]');
    expect(Object.fromEntries(names)).toEqual({
        '#0': 'a',
        '#1': 'b',
    });
});
