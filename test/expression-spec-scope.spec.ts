import { ExpressionSpec, equal, set } from '../src';

test('default', () => {
    const scope: ExpressionSpec.Scope = new ExpressionSpec.Scope();

    expect(scope.evaluate(equal('foo', 'bar'))).toBe('#0 = :0');
    expect(scope.evaluate(set('foo', 'bar'))).toBe('SET #0 = :0');
    expect(scope.names).toEqual({
        '#0': 'foo',
    });
    expect(scope.values).toEqual({
        ':0': 'bar',
    });
});
