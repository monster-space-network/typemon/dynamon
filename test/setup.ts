import { ExpressionSpec } from '../src';

declare global {
    function validate(actual: ExpressionSpec, expected: ExpressionSpec): void;
}

Object.defineProperty(global, 'validate', {
    value(actual: ExpressionSpec, expected: ExpressionSpec): void {
        expect(actual.expression).toBe(expected.expression);
        expect(actual.names).toEqual(expected.names);
        expect(actual.values).toEqual(expected.values);
    },
});
