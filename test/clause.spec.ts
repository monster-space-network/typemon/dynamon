import { Clause } from '../src/clause';

test('stringify', () => {
    expect(Clause.stringify(Clause.Set)).toBe('SET');
    expect(Clause.stringify(Clause.Remove)).toBe('REMOVE');
    expect(Clause.stringify(Clause.Add)).toBe('ADD');
    expect(Clause.stringify(Clause.Delete)).toBe('DELETE');
});
