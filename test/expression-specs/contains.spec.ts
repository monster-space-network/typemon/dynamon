import { contains } from '../../src';

test('default', () => {
    validate(contains('foo', 'bar'), {
        expression: 'contains (#0, :0)',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 'bar',
        },
    });
});
