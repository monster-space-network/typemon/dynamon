import { equal } from '../../src';

test('default', () => {
    validate(equal('foo', 'bar'), {
        expression: '#0 = :0',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 'bar',
        },
    });
});
