import { parenthesize } from '../../src';
import { create } from '../../src/expression-spec';

test('default', () => {
    validate(parenthesize(create(null, 'expression')), {
        expression: '(expression)',
        names: {},
        values: {},
    });
});
