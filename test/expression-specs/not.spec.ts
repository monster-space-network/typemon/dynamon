import { not } from '../../src';
import { create } from '../../src/expression-spec';

test('default', () => {
    validate(not(create(null, 'expression')), {
        expression: 'NOT expression',
        names: {},
        values: {},
    });
});
