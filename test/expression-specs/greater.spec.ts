import { greater } from '../../src';

test('default', () => {
    validate(greater('foo', 'bar'), {
        expression: '#0 > :0',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 'bar',
        },
    });
});
