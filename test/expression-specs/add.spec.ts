import { add } from '../../src';

test('default', () => {
    validate(add('foo', 'bar'), {
        expression: 'ADD #0 :0',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 'bar',
        },
    });
});
