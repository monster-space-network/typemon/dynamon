import { set, plus, minus, listAppend, ifNotExists } from '../../src';

test('default', () => {
    validate(set('foo', 'bar'), {
        expression: 'SET #0 = :0',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 'bar',
        },
    });

    validate(plus('foo', 1), {
        expression: '#0 + :0',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 1,
        },
    });
    validate(minus('foo', 1), {
        expression: '#0 - :0',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 1,
        },
    });

    validate(listAppend('foo', ['bar']), {
        expression: 'list_append (#0, :0)',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': ['bar'],
        },
    });

    validate(ifNotExists('foo', 'bar'), {
        expression: 'if_not_exists (#0, :0)',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 'bar',
        },
    });
});
