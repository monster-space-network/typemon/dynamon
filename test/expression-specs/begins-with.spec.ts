import { beginsWith } from '../../src';

test('default', () => {
    validate(beginsWith('foo', 'bar'), {
        expression: 'begins_with (#0, :0)',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 'bar',
        },
    });
});
