import { attributeNotExists } from '../../src';

test('default', () => {
    validate(attributeNotExists('foo'), {
        expression: 'attribute_not_exists (#0)',
        names: {
            '#0': 'foo',
        },
        values: {},
    });
});
