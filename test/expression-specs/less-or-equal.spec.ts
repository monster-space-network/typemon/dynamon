import { lessOrEqual } from '../../src';

test('default', () => {
    validate(lessOrEqual('foo', 'bar'), {
        expression: '#0 <= :0',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 'bar',
        },
    });
});
