import { del } from '../../src';

test('default', () => {
    validate(del('foo', 'bar'), {
        expression: 'DELETE #0 :0',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 'bar',
        },
    });
});
