import { update, set, remove, add, del } from '../../src';
import { create } from '../../src/expression-spec';

test('empty', () => {
    expect(() => update()).toThrow('Provide at least one update expression specification.');
});

test('invalid', () => {
    expect(() => update(create(null, 'error')).expression).toThrow('Invalid update expression specification.');
});

test('default', () => {
    validate(update(
        set('foo', 'bar'),
        remove('foo'),
        add('foo', 'bar'),
        del('foo', 'bar'),
        [
            set('bar', 'foo'),
            remove('bar'),
            add('bar', 'foo'),
            del('bar', 'foo'),
        ],
    ), {
        expression: [
            'SET #0 = :0, #1 = :1',
            'REMOVE #0, #1',
            'ADD #0 :0, #1 :1',
            'DELETE #0 :0, #1 :1',
        ].join(' '),
        names: {
            '#0': 'foo',
            '#1': 'bar',
        },
        values: {
            ':0': 'bar',
            ':1': 'foo',
        },
    });
});

test('partial', () => {
    validate(update(
        set('foo', 'bar'),
        remove('foo'),
        [
            set('bar', 'foo'),
            remove('bar'),
        ],
    ), {
        expression: [
            'SET #0 = :0, #1 = :1',
            'REMOVE #0, #1',
        ].join(' '),
        names: {
            '#0': 'foo',
            '#1': 'bar',
        },
        values: {
            ':0': 'bar',
            ':1': 'foo',
        },
    });
});
