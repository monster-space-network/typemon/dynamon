import { between } from '../../src';

test('default', () => {
    validate(between('a', 'b', 'c'), {
        expression: '#0 BETWEEN :0 AND :1',
        names: {
            '#0': 'a',
        },
        values: {
            ':0': 'b',
            ':1': 'c',
        },
    });
});
