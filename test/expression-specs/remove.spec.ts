import { remove } from '../../src';

test('empty', () => {
    expect(() => remove()).toThrow('Provide at least one paths.');
});

test('default', () => {
    validate(remove('foo'), {
        expression: 'REMOVE #0',
        names: {
            '#0': 'foo',
        },
        values: {},
    });
});
