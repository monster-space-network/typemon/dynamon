import { attributeType } from '../../src';

test('default', () => {
    validate(attributeType('foo', 'NULL'), {
        expression: 'attribute_type (#0, :0)',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 'NULL',
        },
    });
});
