import { project } from '../../src';

test('empty', () => {
    expect(() => project()).toThrow('Provide at least one path.');
});

test('default', () => {
    validate(project(
        'foo',
        'bar',
        [
            'foo.bar',
            'bar.foo',
        ],
    ), {
        expression: '#0, #1, #0.#1, #1.#0',
        names: {
            '#0': 'foo',
            '#1': 'bar',
        },
        values: {},
    });
});
