import { size, equal, notEqual, less, lessOrEqual, greater, greaterOrEqual, inList, between, contains } from '../../src';

test('default', () => {
    validate(size('foo'), {
        expression: 'size (#0)',
        names: {
            '#0': 'foo',
        },
        values: {},
    });
});

test('equal', () => {
    validate(equal(size('foo'), size('bar')), {
        expression: 'size (#0) = size (#1)',
        names: {
            '#0': 'foo',
            '#1': 'bar',
        },
        values: {},
    });
});
test('not-equal', () => {
    validate(notEqual(size('foo'), size('bar')), {
        expression: 'size (#0) <> size (#1)',
        names: {
            '#0': 'foo',
            '#1': 'bar',
        },
        values: {},
    });
});

test('less', () => {
    validate(less(size('foo'), size('bar')), {
        expression: 'size (#0) < size (#1)',
        names: {
            '#0': 'foo',
            '#1': 'bar',
        },
        values: {},
    });
});
test('less-or-equal', () => {
    validate(lessOrEqual(size('foo'), size('bar')), {
        expression: 'size (#0) <= size (#1)',
        names: {
            '#0': 'foo',
            '#1': 'bar',
        },
        values: {},
    });
});

test('greater', () => {
    validate(greater(size('foo'), size('bar')), {
        expression: 'size (#0) > size (#1)',
        names: {
            '#0': 'foo',
            '#1': 'bar',
        },
        values: {},
    });
});
test('greater-or-equal', () => {
    validate(greaterOrEqual(size('foo'), size('bar')), {
        expression: 'size (#0) >= size (#1)',
        names: {
            '#0': 'foo',
            '#1': 'bar',
        },
        values: {},
    });
});

test('in-list', () => {
    validate(inList(size('foo'), [size('bar')]), {
        expression: 'size (#0) IN (size (#1))',
        names: {
            '#0': 'foo',
            '#1': 'bar',
        },
        values: {},
    });
});

test('between', () => {
    validate(between(size('a'), size('b'), size('c')), {
        expression: 'size (#0) BETWEEN size (#1) AND size (#2)',
        names: {
            '#0': 'a',
            '#1': 'b',
            '#2': 'c',
        },
        values: {},
    });
});

test('contains', () => {
    validate(contains('foo', size('bar')), {
        expression: 'contains (#0, size (#1))',
        names: {
            '#0': 'foo',
            '#1': 'bar',
        },
        values: {},
    });
});
