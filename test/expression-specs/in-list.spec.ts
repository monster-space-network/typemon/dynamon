import { inList } from '../../src';

test('empty', () => {
    expect(() => inList('foo', [])).toThrow('Provide at least one value.');
});

test('default', () => {
    validate(inList('foo', ['bar']), {
        expression: '#0 IN (:0)',
        names: {
            '#0': 'foo',
        },
        values: {
            ':0': 'bar',
        },
    });
});
