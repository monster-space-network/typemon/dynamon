import { attributeExists } from '../../src';

test('default', () => {
    validate(attributeExists('foo'), {
        expression: 'attribute_exists (#0)',
        names: {
            '#0': 'foo',
        },
        values: {},
    });
});
