import { and } from '../../src';
import { create } from '../../src/expression-spec';

test('empty', () => {
    expect(() => and()).toThrow('Provide at least one update expression specification.');
});

test('default', () => {
    validate(and(create(null, 'expression')), {
        expression: 'expression',
        names: {},
        values: {},
    });
    validate(and(create(null, 'a'), create(null, 'b')), {
        expression: 'a AND b',
        names: {},
        values: {},
    });
});

test('parenthesized', () => {
    validate(and.parenthesized(create(null, 'expression')), {
        expression: '(expression)',
        names: {},
        values: {},
    });
    validate(and.parenthesized(create(null, 'a'), create(null, 'b')), {
        expression: '(a AND b)',
        names: {},
        values: {},
    });
});
