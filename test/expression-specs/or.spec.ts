import { or } from '../../src';
import { create } from '../../src/expression-spec';

test('empty', () => {
    expect(() => or()).toThrow('Provide at least one update expression specification.');
});

test('default', () => {
    validate(or(create(null, 'expression')), {
        expression: 'expression',
        names: {},
        values: {},
    });
    validate(or(create(null, 'a'), create(null, 'b')), {
        expression: 'a OR b',
        names: {},
        values: {},
    });
});

test('parenthesized', () => {
    validate(or.parenthesized(create(null, 'expression')), {
        expression: '(expression)',
        names: {},
        values: {},
    });
    validate(or.parenthesized(create(null, 'a'), create(null, 'b')), {
        expression: '(a OR b)',
        names: {},
        values: {},
    });
});
