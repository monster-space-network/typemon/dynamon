import { Check } from '@typemon/check';

import { Names } from './placeholders';

enum State {
    Identifier,
    Index,
    None,
}

enum Token {
    PathDelimiter = '.',
    OpenBracket = '[',
    CloseBracket = ']',
    EscapeCharacter = '\\',
}

interface Context {
    readonly iterator: Iterator<string>;
    index: number;
    current: IteratorResult<string>;
    next: IteratorResult<string>;
    state: State;
    buffer: string;
}
namespace Context {
    export function create(path: string): Context {
        const iterator: Iterator<string> = path[Symbol.iterator]();
        const context: Context = {
            iterator,
            index: 0,
            current: iterator.next(),
            next: iterator.next(),
            state: State.Identifier,
            buffer: '',
        };

        return context;
    }

    export function next(context: Context): void {
        context.index += 1;
        context.current = context.next;
        context.next = context.iterator.next();
    }
}

export class PathSyntaxError extends SyntaxError {
    public constructor(
        public readonly token: string,
        public readonly index: number,
    ) {
        super();

        this.name = this.constructor.name;
        this.message = `Unexpected token '${this.token}' at position ${this.index}.`;
    }
}

export class InvalidIndexError extends Error {
    public constructor(
        public readonly index: string,
    ) {
        super();

        this.name = this.constructor.name;
        this.message = `Index '${this.index}' is not valid.`;
    }
}

export namespace PathParser {
    export interface Element {
        readonly value: string;
        readonly index: null | string;
    }

    export function join(elements: ReadonlyArray<Element>, names: Names): string {
        return elements
            .map((element: Element): string => {
                const placeholder: string = names.add(element.value);

                if (Check.isNull(element.index)) {
                    return placeholder;
                }

                return placeholder.concat(element.index);
            })
            .join('.');
    }

    export function parse(path: string): ReadonlyArray<Element> {
        if (Check.equal(path.length, 0)) {
            throw new PathSyntaxError('', 0);
        }

        const context: Context = Context.create(path);
        const elements: Array<Element> = [];

        while (Check.isFalse(context.current.done)) {
            if (Check.equal(context.state, State.Identifier)) {
                switch (context.current.value) {
                    // 닫는 대괄호로 시작하는 경우 구문 오류를 던짐
                    case Token.CloseBracket: throw new PathSyntaxError(context.current.value, context.index);

                    case Token.OpenBracket: {
                        // 여는 대괄호로 시작하는 경우 구문 오류를 던짐
                        if (Check.equal(elements.length, 0) && Check.equal(context.buffer.length, 0)) {
                            throw new PathSyntaxError(context.current.value, context.index);
                        }

                        // 여는 대괄호로 끝나는 경우 구문 오류를 던짐
                        if (context.next.done) {
                            throw new PathSyntaxError(context.current.value, context.index);
                        }

                        context.state = State.Index;
                    }

                    case Token.PathDelimiter: {
                        // 마침표로 끝나는 경우 구문 오류를 던짐
                        if (context.next.done) {
                            throw new PathSyntaxError(context.current.value, context.index);
                        }

                        // 마침표로 시작하는 경우 구문 오류를 던짐
                        if (Check.equal(context.buffer.length, 0)) {
                            throw new PathSyntaxError(context.current.value, context.index);
                        }

                        elements.push({
                            value: context.buffer,
                            index: null,
                        });
                        context.buffer = '';

                        break;
                    }

                    case Token.EscapeCharacter: {
                        // 백슬래시로 끝나는 경우 구문 오류를 던짐
                        if (context.next.done) {
                            throw new PathSyntaxError(context.current.value, context.index);
                        }

                        if (Check.equal(context.next.value, Token.PathDelimiter) ||
                            Check.equal(context.next.value, Token.OpenBracket) ||
                            Check.equal(context.next.value, Token.CloseBracket) ||
                            Check.equal(context.next.value, Token.EscapeCharacter)) {
                            Context.next(context);
                        }
                    }

                    default: {
                        context.buffer += context.current.value;
                    }
                }
            }
            else if (Check.equal(context.state, State.Index)) {
                switch (context.current.value) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9': {
                        // 숫자로 끝나는 경우 구문 오류를 던짐
                        if (context.next.done) {
                            throw new PathSyntaxError(context.current.value, context.index);
                        }

                        context.buffer += context.current.value;

                        break;
                    }

                    case Token.CloseBracket: {
                        // 인덱스 없이 대괄호가 닫히면 구문 오류를 던짐
                        if (Check.equal(context.buffer.length, 0)) {
                            throw new PathSyntaxError(context.current.value, context.index);
                        }

                        const index: string = Number
                            .parseInt(context.buffer, 10)
                            .toString();

                        // 인덱스가 유효하지 않은 경우 오류를 던짐
                        if (Check.notEqual(index, context.buffer)) {
                            throw new InvalidIndexError(context.buffer);
                        }

                        const previousElement: Element = elements.pop() as Element;

                        elements.push({
                            value: previousElement.value,
                            index: (previousElement.index ?? '').concat(`[${index}]`),
                        });
                        context.state = State.None;
                        context.buffer = '';

                        break;
                    }

                    // 숫자 또는 닫는 대괄호가 아닐 경우 구문 오류를 던짐
                    default: throw new PathSyntaxError(context.current.value, context.index);
                }
            }
            else if (Check.isFalse(context.next.done)) {
                if (Check.equal(context.current.value, Token.PathDelimiter)) {
                    context.state = State.Identifier;
                }
                else if (Check.equal(context.current.value, Token.OpenBracket)) {
                    context.state = State.Index;
                }
                // 상태가 지정되지 않았을 경우 구문 오류를 던짐
                else {
                    throw new PathSyntaxError(context.current.value, context.index);
                }
            }
            // 상태가 지정되지 않았을 경우 구문 오류를 던짐
            else {
                throw new PathSyntaxError(context.current.value, context.index);
            }

            Context.next(context);
        }

        if (Check.greater(context.buffer.length, 0)) {
            elements.push({
                value: context.buffer,
                index: null,
            });
        }

        return elements;
    }
}
