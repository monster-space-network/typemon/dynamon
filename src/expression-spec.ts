import { Check } from '@typemon/check';

import { Clause } from './clause';
import { Placeholders, Names, Values } from './placeholders';

export interface ExpressionSpec {
    readonly expression: string;
    readonly names: {
        readonly [placeholder: string]: string;
    };
    readonly values: {
        readonly [placeholder: string]: unknown;
    };
}
export namespace ExpressionSpec {
    export class Scope {
        private readonly names$: Names;
        private readonly values$: Values;

        public constructor() {
            this.names$ = new Placeholders('#');
            this.values$ = new Placeholders(':');
        }

        public get names(): Readonly<Record<string, string>> {
            return Object.fromEntries(this.names$);
        }

        public get values(): Readonly<Record<string, unknown>> {
            return Object.fromEntries(this.values$);
        }

        public evaluate(expressionSpec: ExpressionSpec): string {
            const expression: string = express(expressionSpec, this.names$, this.values$);
            const clause: null | Clause = getClause(expressionSpec);

            if (Check.isNull(clause)) {
                return expression;
            }

            return `${Clause.stringify(clause)} ${expression}`;
        }
    }
}

export const identifier: unique symbol = Symbol('typemon.dynamon.expression-spec');

export function isExpressionSpec(value: unknown): value is ExpressionSpec {
    return Check.isObject(value) && Check.equal((value as any)[identifier], identifier);
}

export type Expressor = (names: Names, values: Values) => string;

export function create(clause: null | Clause, expressible: string | Expressor | ExpressionSpec): ExpressionSpec {
    const expressor: Expressor = Check.isString(expressible)
        ? (): string => expressible
        : Check.isFunction(expressible)
            ? expressible
            : (expressible as any).expressor;
    let expressed: boolean = false;

    return new Proxy({}, {
        get(cache: any, key: PropertyKey): any {
            if (Check.equal(key, identifier)) {
                return identifier;
            }

            if (Check.equal(key, 'expressor')) {
                return expressor;
            }

            if (Check.equal(key, 'clause')) {
                return clause;
            }

            if (Check.isFalse(expressed)) {
                const names: Names = new Placeholders('#');
                const values: Values = new Placeholders(':');
                const expression: string = expressor(names, values);

                cache.expression = Check.isNull(clause)
                    ? expression
                    : `${Clause.stringify(clause)} ${expression}`;
                cache.names = Object.fromEntries(names);
                cache.values = Object.fromEntries(values);
                expressed = true;
            }

            return cache[key];
        },
    });
}

export function express(expressionSpec: any, names: Names, values: Values): string {
    return expressionSpec.expressor(names, values);
}

export function getClause(expressionSpec: any): null | Clause {
    return expressionSpec.clause;
}
