export enum Clause {
    Set,
    Remove,
    Add,
    Delete,
}
export namespace Clause {
    export function stringify(clause: Clause): string {
        return Clause[clause].toUpperCase();
    }
}
