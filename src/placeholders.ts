export class Placeholders<Value> implements Iterable<[string, Value]> {
    private readonly map: Map<Value, string>;
    private nextId: number;

    public constructor(
        private readonly prefix: string,
    ) {
        this.map = new Map();
        this.nextId = 0;
    }

    public *[Symbol.iterator](): Iterator<[string, Value]> {
        for (const [value, placeholder] of this.map) {
            yield [placeholder, value];
        }
    }

    public add(value: Value): string {
        if (this.map.has(value)) {
            return this.map.get(value) as string;
        }
        else {
            const placeholder: string = `${this.prefix}${this.nextId++}`;

            this.map.set(value, placeholder);

            return placeholder;
        }
    }
}

export type Names = Placeholders<string>;
export type Values = Placeholders<unknown>;
