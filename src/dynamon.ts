import { Check } from '@typemon/check';
import * as Marshaller from '@aws-sdk/util-dynamodb';
import * as API from '@aws-sdk/client-dynamodb';

import { ExpressionSpec } from './expression-spec';

function sanitize(value: undefined | null | object): any {
    if (Check.isUndefinedOrNull(value)) {
        return undefined;
    }

    const keys: ReadonlyArray<string> = Object.keys(value);

    if (Check.equal(keys.length, 0)) {
        return undefined;
    }

    return value;
}

function $marshall(data: any): undefined | Marshalled {
    const sanitized: undefined | object = sanitize(data);

    if (Check.isUndefined(sanitized)) {
        return undefined;
    }

    return Dynamon.marshall(sanitized);
}

function $unmarshall(data: any): null | object {
    if (Check.isUndefined(data)) {
        return null;
    }

    return Dynamon.unmarshall(data);
}

interface Marshalled {
    readonly [key: string]: API.AttributeValue;
}

interface TableName {
    readonly tableName: string;
}

interface PrimaryKey {
    readonly primaryKey: object;
}

interface ConditionExpressionSpec {
    readonly conditionExpressionSpec?: null | ExpressionSpec;
}

interface ProjectionExpressionSpec {
    readonly projectionExpressionSpec?: null | ExpressionSpec;
}

interface FilterExpressionSpec {
    readonly filterExpressionSpec?: null | ExpressionSpec;
}

interface ConsistentRead {
    readonly consistentRead?: boolean;
}

interface ReturnValuesOnConditionCheckFailure {
    readonly returnValuesOnConditionCheckFailure?: 'NONE' | 'ALL_OLD';
}

interface IndexName {
    readonly indexName?: string;
}

interface Pagination {
    readonly limit?: number;
    readonly exclusiveStartKey?: null | object;

    /**
     * @default false
     */
    readonly skipEmptyPage?: boolean;

    readonly concatenateWhile?: Dynamon.Predicate;
}

function* chunky<Item>(size: number, items: ReadonlyArray<Item>): Generator<ReadonlyArray<Item>> {
    for (let index: number = 0; Check.less(index * size, items.length); index++) {
        yield items.slice(index * size, index * size + size);
    }
}

function empty(paginator: Dynamon.Paginator): Dynamon.Page {
    return {
        items: [],
        lastEvaluatedKey: null,
        count: paginator.count,
        scannedCount: paginator.scannedCount,
        index: paginator.index,
    };
}

class Paginator implements Dynamon.Paginator {
    public used: boolean;
    public count: number;
    public scannedCount: number;
    public index: number;

    public constructor(
        private readonly options: Pagination,
        private readonly next: (exclusiveStartKey: undefined | Marshalled) => Promise<API.QueryCommandOutput | API.ScanCommandOutput>,
    ) {
        this.used = false;
        this.count = 0;
        this.scannedCount = 0;
        this.index = -1;
    }

    public get unused(): boolean {
        return Check.isFalse(this.used);
    }

    public async *[Symbol.asyncIterator](): AsyncGenerator<Dynamon.Page> {
        if (this.used) {
            throw new Error('Paginator is disposable. Do not reuse.');
        }

        this.used = true;

        let items: Array<Marshalled> = [];
        let exclusiveStartKey: undefined | Marshalled = $marshall(this.options.exclusiveStartKey);
        let count: number = 0;
        let scannedCount: number = 0;
        let index: number = -1;

        do {
            const output: API.QueryCommandOutput | API.ScanCommandOutput = await this.next(exclusiveStartKey);

            this.count += output.Count!;
            this.scannedCount += output.ScannedCount!;
            this.index += 1;

            items.push(...output.Items!);
            exclusiveStartKey = output.LastEvaluatedKey;
            count += output.Count!;
            scannedCount += output.ScannedCount!;
            index += 1;

            if (Check.isTrue(this.options.skipEmptyPage) && Check.equal(count, 0)) {
                continue;
            }

            const concatenated: Dynamon.Page = {
                items: items.map(Dynamon.unmarshall),
                lastEvaluatedKey: $unmarshall(exclusiveStartKey),
                count,
                scannedCount,
                index: this.index,
            };

            if (Check.isUndefined(this.options.concatenateWhile) || Check.isUndefined(exclusiveStartKey)) {
                yield concatenated;
            }
            else {
                if (this.options.concatenateWhile(concatenated, index)) {
                    continue;
                }

                yield concatenated;
            }

            items = [];
            count = 0;
            scannedCount = 0;
            index = -1;
        } while (Check.isNotUndefined(exclusiveStartKey));
    }
}

export class Dynamon {
    public static marshall(data: object): Marshalled {
        return Marshaller.marshall(data);
    }

    public static unmarshall(data: Marshalled): object {
        return Marshaller.unmarshall(data);
    }

    public constructor(
        protected readonly client: API.DynamoDBClient = new API.DynamoDBClient({}),
    ) { }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/WorkingWithItems.html#WorkingWithItems.WritingData
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_PutItem.html
     */
    public async put(data: Dynamon.Put): Promise<any> {
        const input: API.PutItemInput = {
            TableName: data.tableName,
            Item: Dynamon.marshall(data.item),
            ReturnValues: data.returnValues,
        };

        if (Check.isNotUndefinedAndNotNull(data.conditionExpressionSpec)) {
            input.ConditionExpression = data.conditionExpressionSpec.expression;
            input.ExpressionAttributeNames = data.conditionExpressionSpec.names;
            input.ExpressionAttributeValues = $marshall(data.conditionExpressionSpec.values);
        }

        return this.client
            .send(new API.PutItemCommand(input))
            .then((output: API.PutItemCommandOutput): unknown => $unmarshall(output.Attributes));
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/WorkingWithItems.html#WorkingWithItems.ReadingData
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_GetItem.html
     */
    public async get(data: Dynamon.Get): Promise<any> {
        const input: API.GetItemInput = {
            TableName: data.tableName,
            Key: Dynamon.marshall(data.primaryKey),
            ConsistentRead: data.consistentRead,
        };

        if (Check.isNotUndefinedAndNotNull(data.projectionExpressionSpec)) {
            input.ProjectionExpression = data.projectionExpressionSpec.expression;
            input.ExpressionAttributeNames = data.projectionExpressionSpec.names;
        }

        return this.client
            .send(new API.GetItemCommand(input))
            .then((output: API.GetItemCommandOutput): unknown => $unmarshall(output.Item));
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/WorkingWithItems.html#WorkingWithItems.WritingData
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_UpdateItem.html
     */
    public async update(data: Dynamon.Update): Promise<any> {
        const scope: ExpressionSpec.Scope = new ExpressionSpec.Scope();
        const input: API.UpdateItemInput = {
            TableName: data.tableName,
            Key: Dynamon.marshall(data.primaryKey),
            ReturnValues: data.returnValues,
        };

        if (Check.isNotUndefinedAndNotNull(data.conditionExpressionSpec)) {
            input.ConditionExpression = scope.evaluate(data.conditionExpressionSpec);
        }

        if (Check.isNotUndefinedAndNotNull(data.updateExpressionSpec)) {
            input.UpdateExpression = scope.evaluate(data.updateExpressionSpec);
        }

        input.ExpressionAttributeNames = sanitize(scope.names);
        input.ExpressionAttributeValues = $marshall(scope.values);

        return this.client
            .send(new API.UpdateItemCommand(input))
            .then((output: API.UpdateItemCommandOutput): unknown => $unmarshall(output.Attributes));
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/WorkingWithItems.html#WorkingWithItems.WritingData
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_DeleteItem.html
     */
    public async delete(data: Dynamon.Delete): Promise<any> {
        const input: API.DeleteItemInput = {
            TableName: data.tableName,
            Key: Dynamon.marshall(data.primaryKey),
            ReturnValues: data.returnValues,
        };

        if (Check.isNotUndefinedAndNotNull(data.conditionExpressionSpec)) {
            input.ConditionExpression = data.conditionExpressionSpec.expression;
            input.ExpressionAttributeNames = data.conditionExpressionSpec.names;
            input.ExpressionAttributeValues = $marshall(data.conditionExpressionSpec.values);
        }

        return this.client
            .send(new API.DeleteItemCommand(input))
            .then((output: API.DeleteItemCommandOutput): unknown => $unmarshall(output.Attributes));
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Query.html
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Query.html
     */
    public async query(data: Dynamon.Query): Promise<Dynamon.Page> {
        const paginator: Dynamon.Paginator = this.query$(data);

        for await (const page of paginator) {
            return page;
        }

        return empty(paginator);
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Query.html
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Query.html
     */
    public query$(data: Dynamon.Query): Dynamon.Paginator {
        const scope: ExpressionSpec.Scope = new ExpressionSpec.Scope();
        const input: API.QueryInput = {
            TableName: data.tableName,
            ConsistentRead: data.consistentRead,
            IndexName: data.indexName,
            ScanIndexForward: data.scanIndexForward,
            KeyConditionExpression: scope.evaluate(data.keyConditionExpressionSpec),
            Limit: data.limit,
        };

        if (Check.isNotUndefinedAndNotNull(data.filterExpressionSpec)) {
            input.FilterExpression = scope.evaluate(data.filterExpressionSpec);
        }

        if (Check.isNotUndefinedAndNotNull(data.projectionExpressionSpec)) {
            input.ProjectionExpression = scope.evaluate(data.projectionExpressionSpec);
        }

        input.ExpressionAttributeNames = scope.names;
        input.ExpressionAttributeValues = Dynamon.marshall(scope.values);

        return new Paginator(data, (exclusiveStartKey: undefined | Marshalled): Promise<API.QueryCommandOutput> => {
            const command: API.QueryCommand = new API.QueryCommand({
                ...input,
                ExclusiveStartKey: exclusiveStartKey,
            });

            return this.client.send(command);
        });
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Query.html
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Query.html
     */
    public async queryAll(data: Dynamon.Query): Promise<ReadonlyArray<any>> {
        const items: Array<unknown> = [];

        for await (const page of this.query$(data)) {
            items.push(...page.items);
        }

        return items;
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Scan.html
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Scan.html
     */
    public async scan(data: Dynamon.Scan): Promise<Dynamon.Page> {
        const paginator: Dynamon.Paginator = this.scan$(data);

        for await (const page of paginator) {
            return page;
        }

        return empty(paginator);
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Scan.html
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Scan.html
     */
    public scan$(data: Dynamon.Scan): Dynamon.Paginator {
        const scope: ExpressionSpec.Scope = new ExpressionSpec.Scope();
        const input: API.ScanInput = {
            TableName: data.tableName,
            ConsistentRead: data.consistentRead,
            IndexName: data.indexName,
            Limit: data.limit,
            Segment: data.segment,
            TotalSegments: data.totalSegments,
        };

        if (Check.isNotUndefinedAndNotNull(data.filterExpressionSpec)) {
            input.FilterExpression = scope.evaluate(data.filterExpressionSpec);
        }

        if (Check.isNotUndefinedAndNotNull(data.projectionExpressionSpec)) {
            input.ProjectionExpression = scope.evaluate(data.projectionExpressionSpec);
        }

        input.ExpressionAttributeNames = sanitize(scope.names);
        input.ExpressionAttributeValues = $marshall(scope.values);

        return new Paginator(data, (exclusiveStartKey: undefined | Marshalled): Promise<API.ScanCommandOutput> => {
            const command: API.ScanCommand = new API.ScanCommand({
                ...input,
                ExclusiveStartKey: exclusiveStartKey,
            });

            return this.client.send(command);
        });
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Scan.html
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Scan.html
     */
    public async scanAll(data: Dynamon.Scan): Promise<ReadonlyArray<any>> {
        const items: Array<unknown> = [];

        for await (const page of this.scan$(data)) {
            items.push(...page.items);
        }

        return items;
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/WorkingWithItems.html#WorkingWithItems.BatchOperations
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_BatchGetItem.html
     */
    public async batchGet(data: Dynamon.BatchGet): Promise<Dynamon.BatchGet.Result> {
        const flattedOperations: ReadonlyArray<TableName & PrimaryKey> = Object
            .entries(data)
            .flatMap(([tableName, operation]: [string, Dynamon.BatchGet.Operation]): ReadonlyArray<TableName & PrimaryKey> => operation.primaryKeys.map((primaryKey: object): TableName & PrimaryKey => ({ tableName, primaryKey })))
        const responses: Map<string, ReadonlyArray<unknown>> = new Map();
        const unprocesseds: Map<string, ReadonlyArray<unknown>> = new Map();

        for (const chunk of chunky(100, flattedOperations)) {
            const primaryKeys: Map<string, ReadonlyArray<object>> = new Map();

            for (const { tableName, primaryKey } of chunk) {
                const table: ReadonlyArray<object> = primaryKeys.get(tableName) ?? [];

                primaryKeys.set(tableName, table.concat(primaryKey));
            }

            const requestItems: ReadonlyArray<[string, API.KeysAndAttributes]> = Array
                .from(primaryKeys)
                .map(([tableName, primaryKeys]: [string, ReadonlyArray<object>]): [string, API.KeysAndAttributes] => {
                    const operation: Dynamon.BatchGet.Operation = data[tableName];
                    const request: API.KeysAndAttributes = {
                        Keys: primaryKeys.map(Dynamon.marshall),
                        ConsistentRead: operation.consistentRead,
                    };

                    if (Check.isNotUndefinedAndNotNull(operation.projectionExpressionSpec)) {
                        request.ProjectionExpression = operation.projectionExpressionSpec.expression;
                        request.ExpressionAttributeNames = operation.projectionExpressionSpec.names;
                    }

                    return [tableName, request];
                });
            const input: API.BatchGetItemInput = {
                RequestItems: Object.fromEntries(requestItems),
            };
            const output: API.BatchGetItemCommandOutput = await this.client.send(new API.BatchGetItemCommand(input));

            for (const [tableName, items] of Object.entries(output.Responses!)) {
                const unmarshalledItems: ReadonlyArray<unknown> = items.map(Dynamon.unmarshall);
                const table: ReadonlyArray<unknown> = responses.get(tableName) ?? [];

                responses.set(tableName, table.concat(unmarshalledItems));
            }

            for (const [tableName, request] of Object.entries(output.UnprocessedKeys!)) {
                const unmarshalledPrimaryKeys: ReadonlyArray<unknown> = request.Keys!.map(Dynamon.unmarshall);
                const table: ReadonlyArray<unknown> = unprocesseds.get(tableName) ?? [];

                unprocesseds.set(tableName, table.concat(unmarshalledPrimaryKeys));
            }
        }

        const unprocessedEntries: ReadonlyArray<[string, Dynamon.BatchGet.Operation]> = Object
            .entries(unprocesseds)
            .map(([tableName, primaryKeys]: [string, ReadonlyArray<object>]): [string, Dynamon.BatchGet.Operation] => {
                const operation: Dynamon.BatchGet.Operation = {
                    ...data[tableName],
                    primaryKeys,
                };

                return [tableName, operation];
            });
        const result: Dynamon.BatchGet.Result = {
            responses: Object.fromEntries(responses),
            unprocessed: Check.greater(unprocessedEntries.length, 0)
                ? Object.fromEntries(unprocessedEntries)
                : null,
        };

        return result;
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/WorkingWithItems.html#WorkingWithItems.BatchOperations
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_BatchWriteItem.html
     */
    public async batchWrite(data: Dynamon.BatchWrite): Promise<null | Dynamon.BatchWrite> {
        const flattedOperations: ReadonlyArray<TableName & API.WriteRequest> = Object
            .entries(data)
            .flatMap(([tableName, operations]: [string, ReadonlyArray<Dynamon.BatchWrite.Put | Dynamon.BatchWrite.Delete>]): ReadonlyArray<TableName & API.WriteRequest> => operations.map((operation: any): TableName & API.WriteRequest => {
                const request: Partial<API.PutRequest & API.DeleteRequest> = {};
                const type: string = `${operation.type}Request`;

                if (Check.equal(type, 'PutRequest')) {
                    request.Item = Dynamon.marshall(operation.item);
                }
                else {
                    request.Key = Dynamon.marshall(operation.primaryKey);
                }

                return {
                    tableName,
                    [type]: request,
                };
            }))
        const unprocessedItems: Map<string, ReadonlyArray<Dynamon.BatchWrite.Put | Dynamon.BatchWrite.Delete>> = new Map();

        for (const chunk of chunky(25, flattedOperations)) {
            const requestItems: Map<string, Array<API.WriteRequest>> = new Map();

            for (const { tableName, ...requestItem } of chunk) {
                const table: ReadonlyArray<API.WriteRequest> = requestItems.get(tableName) ?? [];

                requestItems.set(tableName, table.concat(requestItem));
            }

            const input: API.BatchWriteItemInput = {
                RequestItems: Object.fromEntries(requestItems),
            };
            const output: API.BatchWriteItemCommandOutput = await this.client.send(new API.BatchWriteItemCommand(input));

            for (const [tableName, requests] of Object.entries(output.UnprocessedItems!)) {
                const operations: ReadonlyArray<Dynamon.BatchWrite.Put | Dynamon.BatchWrite.Delete> = requests.map((request: any): Dynamon.BatchWrite.Put | Dynamon.BatchWrite.Delete => {
                    const operation: any = {};

                    if ('PutRequest' in request) {
                        operation.type = 'Put';
                        operation.item = Dynamon.unmarshall(request.PutRequest.Item);
                    }
                    else {
                        operation.type = 'Delete';
                        operation.primaryKey = Dynamon.unmarshall(request.DeleteRequest.Key);
                    }

                    return operation;
                });
                const table: ReadonlyArray<Dynamon.BatchWrite.Put | Dynamon.BatchWrite.Delete> = unprocessedItems.get(tableName) ?? [];

                unprocessedItems.set(tableName, table.concat(operations));
            }
        }

        if (Check.equal(unprocessedItems.size, 0)) {
            return null;
        }

        return Object.fromEntries(unprocessedItems);
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/transaction-apis.html#transaction-apis-txwriteitems
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_TransactWriteItems.html
     */
    public async transactWrite(operations: ReadonlyArray<Dynamon.TransactWrite>): Promise<void> {
        const input: API.TransactWriteItemsInput = {
            TransactItems: operations.map((operation: any): API.TransactWriteItem => {
                const scope: ExpressionSpec.Scope = new ExpressionSpec.Scope();
                const input: Partial<API.ConditionCheck & API.Put & API.Update & API.Delete> = {
                    TableName: operation.tableName,
                    ReturnValuesOnConditionCheckFailure: operation.returnValuesOnConditionCheckFailure,
                };

                if (Check.isNotUndefined(operation.item)) {
                    input.Item = Dynamon.marshall(operation.item);
                }

                if (Check.isNotUndefined(operation.primaryKey)) {
                    input.Key = Dynamon.marshall(operation.primaryKey);
                }

                if (Check.isNotUndefinedAndNotNull(operation.conditionExpressionSpec)) {
                    input.ConditionExpression = scope.evaluate(operation.conditionExpressionSpec);
                }

                if (Check.isNotUndefinedAndNotNull(operation.updateExpressionSpec)) {
                    input.UpdateExpression = scope.evaluate(operation.updateExpressionSpec);
                }

                input.ExpressionAttributeNames = sanitize(scope.names);
                input.ExpressionAttributeValues = $marshall(scope.values);

                return {
                    [operation.type]: input,
                };
            }),
        };

        await this.client.send(new API.TransactWriteItemsCommand(input));
    }

    /**
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/transaction-apis.html#transaction-apis-txgetitems
     * @link https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_TransactGetItems.html
     */
    public async transactGet(operations: ReadonlyArray<Dynamon.TransactGet>): Promise<ReadonlyArray<any>> {
        const input: API.TransactGetItemsInput = {
            TransactItems: operations.map((operation: Dynamon.TransactGet): API.TransactGetItem => {
                const input: API.GetItemInput = {
                    TableName: operation.tableName,
                    Key: Dynamon.marshall(operation.primaryKey),
                };

                if (Check.isNotUndefinedAndNotNull(operation.projectionExpressionSpec)) {
                    input.ProjectionExpression = operation.projectionExpressionSpec.expression;
                    input.ExpressionAttributeNames = operation.projectionExpressionSpec.names;
                }

                return {
                    Get: input,
                };
            }),
        };

        return this.client
            .send(new API.TransactGetItemsCommand(input))
            .then((output: API.TransactGetItemsCommandOutput): ReadonlyArray<API.ItemResponse> => output.Responses!)
            .then((responses: ReadonlyArray<API.ItemResponse>): ReadonlyArray<unknown> => responses.map((response: API.ItemResponse): unknown => $unmarshall(response.Item)));
    }
}
export namespace Dynamon {
    export interface Put extends TableName, ConditionExpressionSpec {
        readonly item: object;
        readonly returnValues?: 'NONE' | 'ALL_OLD';
    }

    export interface Get extends TableName, PrimaryKey, ProjectionExpressionSpec, ConsistentRead { }

    export interface Update extends TableName, PrimaryKey, ConditionExpressionSpec {
        readonly updateExpressionSpec?: null | ExpressionSpec;
        readonly returnValues?: 'NONE' | 'ALL_OLD' | 'UPDATED_OLD' | 'ALL_NEW' | 'UPDATED_NEW';
    }

    export interface Delete extends TableName, PrimaryKey, ConditionExpressionSpec {
        readonly returnValues?: 'NONE' | 'ALL_OLD';
    }

    export interface Query extends TableName, ProjectionExpressionSpec, FilterExpressionSpec, ConsistentRead, IndexName, Pagination {
        readonly scanIndexForward?: boolean;
        readonly keyConditionExpressionSpec: ExpressionSpec;
    }

    export interface Scan extends TableName, ProjectionExpressionSpec, FilterExpressionSpec, ConsistentRead, IndexName, Pagination {
        readonly segment?: number;
        readonly totalSegments?: number;
    }

    export interface Page<Item extends object = any> {
        readonly items: ReadonlyArray<Item>;
        readonly lastEvaluatedKey: null | object;
        readonly count: number;
        readonly scannedCount: number;
        readonly index: number;
    }

    export interface Paginator<Item extends object = any> extends AsyncIterable<Page<Item>> {
        readonly used: boolean;
        readonly unused: boolean;

        readonly count: number;
        readonly scannedCount: number;
        readonly index: number;
    }

    export type Predicate = (page: Page, index: number) => boolean;

    export interface BatchGet {
        readonly [tableName: string]: BatchGet.Operation;
    }
    export namespace BatchGet {
        export interface Operation extends ProjectionExpressionSpec, ConsistentRead {
            readonly primaryKeys: ReadonlyArray<object>;
        }

        export interface Result {
            readonly responses: Result.Responses;
            readonly unprocessed: null | BatchGet;
        }
        export namespace Result {
            export interface Responses {
                readonly [tableName: string]: ReadonlyArray<any>;
            }
        }
    }

    export interface BatchWrite {
        readonly [tableName: string]: ReadonlyArray<BatchWrite.Put | BatchWrite.Delete>;
    }
    export namespace BatchWrite {
        export interface Put {
            readonly type: 'Put';
            readonly item: object;
        }

        export interface Delete extends PrimaryKey {
            readonly type: 'Delete';
        }
    }

    export type TransactWrite = TransactWrite.ConditionCheck | TransactWrite.Put | TransactWrite.Update | TransactWrite.Delete;
    export namespace TransactWrite {
        export interface ConditionCheck extends TableName, PrimaryKey, ReturnValuesOnConditionCheckFailure {
            readonly type: 'ConditionCheck';
            readonly conditionExpressionSpec: ExpressionSpec;
        }

        export interface Put extends TableName, ConditionExpressionSpec, ReturnValuesOnConditionCheckFailure {
            readonly type: 'Put';
            readonly item: object;
        }

        export interface Update extends TableName, PrimaryKey, ConditionExpressionSpec, ReturnValuesOnConditionCheckFailure {
            readonly type: 'Update';
            readonly updateExpressionSpec: ExpressionSpec;
        }

        export interface Delete extends TableName, PrimaryKey, ConditionExpressionSpec, ReturnValuesOnConditionCheckFailure {
            readonly type: 'Delete';
        }
    }

    export interface TransactGet extends TableName, PrimaryKey, ProjectionExpressionSpec { }
}
