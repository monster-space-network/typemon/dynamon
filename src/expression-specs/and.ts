import { Check } from '@typemon/check';

import { ExpressionSpec } from '../expression-spec';
import { join } from './join';
import { parenthesize } from './parenthesize';

/**
 * `expression AND expression AND expression ...`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.LogicalEvaluations
 */
export function and(...expressionSpecs: ReadonlyArray<ExpressionSpec | ReadonlyArray<ExpressionSpec>>): ExpressionSpec {
    const flattenedExpressionSpecs: ReadonlyArray<ExpressionSpec> = expressionSpecs.flat();

    if (Check.equal(flattenedExpressionSpecs.length, 0)) {
        throw new Error('Provide at least one update expression specification.');
    }

    return join(' AND ', flattenedExpressionSpecs);
}
export namespace and {
    /**
     * `(expression AND expression AND expression ...)`
     *
     * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.LogicalEvaluations
     */
    export function parenthesized(...expressionSpecs: ReadonlyArray<ExpressionSpec | ReadonlyArray<ExpressionSpec>>): ExpressionSpec {
        return parenthesize(and(...expressionSpecs));
    }
}
