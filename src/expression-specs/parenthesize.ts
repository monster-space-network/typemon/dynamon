import { ExpressionSpec, create, express } from '../expression-spec';
import { Names, Values } from '../placeholders';

/**
 * `(expression)`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Parentheses
 */
export function parenthesize(expressionSpec: ExpressionSpec): ExpressionSpec {
    return create(null, (names: Names, values: Values): string => `(${express(expressionSpec, names, values)})`);
}
