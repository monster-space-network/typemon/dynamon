import { ExpressionSpec, create, express } from '../expression-spec';
import { Names, Values } from '../placeholders';

export function join(separator: string, expressionSpecs: ReadonlyArray<ExpressionSpec>): ExpressionSpec {
    return create(null, (names: Names, values: Values): string => expressionSpecs
        .map((expressionSpec: ExpressionSpec): string => express(expressionSpec, names, values))
        .join(separator));
}
