import { ExpressionSpec } from '../expression-spec';
import { func } from './func';
import { parse } from './parse';
import { substitute } from './substitute';

/**
 * `attribute_type (path, type)`
 *
 * @param path `parse`
 * @param type `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Functions
 */
export function attributeType(path: string | ExpressionSpec, type: 'S' | 'SS' | 'N' | 'NS' | 'B' | 'BS' | 'BOOL' | 'NULL' | 'L' | 'M'): ExpressionSpec {
    return func('attribute_type', [
        parse(path),
        substitute(type),
    ]);
}
