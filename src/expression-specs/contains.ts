import { ExpressionSpec } from '../expression-spec';
import { func } from './func';
import { parse } from './parse';
import { substitute } from './substitute';

/**
 * `contains (path, value)`
 *
 * @param path `parse`
 * @param value `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Functions
 */
export function contains(path: string | ExpressionSpec, value: unknown): ExpressionSpec {
    return func('contains', [
        parse(path),
        substitute(value),
    ]);
}
