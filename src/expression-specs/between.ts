import { ExpressionSpec } from '../expression-spec';
import { join } from './join';
import { parse } from './parse';
import { and } from './and';
import { substitute } from './substitute';

/**
 * `a BETWEEN b AND c`
 *
 * @param a `parse`
 * @param b `substitute`
 * @param c `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Comparators
 */
export function between(a: string | ExpressionSpec, b: unknown, c: unknown): ExpressionSpec {
    return join(' BETWEEN ', [
        parse(a),
        and(
            substitute(b),
            substitute(c),
        ),
    ]);
}
