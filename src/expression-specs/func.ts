import { ExpressionSpec, create } from '../expression-spec';
import { join } from './join';
import { parenthesize } from './parenthesize';

export function func(name: string, expressionSpecs: ReadonlyArray<ExpressionSpec>): ExpressionSpec {
    return join(' ', [
        create(null, name),
        parenthesize(join(', ', expressionSpecs)),
    ]);
}
