import { ExpressionSpec, create } from '../expression-spec';
import { Clause } from '../clause';
import { parse } from './parse';
import { join } from './join';
import { substitute } from './substitute';

/**
 * `ADD path value`
 *
 * @param path `parse`
 * @param value `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.ADD
 */
export function add(path: string | ExpressionSpec, value: unknown): ExpressionSpec {
    return create(Clause.Add, join(' ', [
        parse(path),
        substitute(value),
    ]));
}
