import { Check } from '@typemon/check';

import { ExpressionSpec, isExpressionSpec, create } from '../expression-spec';
import { Names } from '../placeholders';
import { PathParser } from '../path-parser';

/**
 * `#path`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.ExpressionAttributeNames.html
 */
export function parse(path: string | ExpressionSpec): ExpressionSpec {
    if (isExpressionSpec(path)) {
        return path;
    }

    let cachedElements: null | ReadonlyArray<PathParser.Element> = null;

    return create(null, (names: Names): string => {
        if (Check.isNull(cachedElements)) {
            cachedElements = PathParser.parse(path);
        }

        return PathParser.join(cachedElements, names);
    });
}
