import { ExpressionSpec, create } from '../expression-spec';
import { Clause } from '../clause';
import { equal } from './equal';
import { join } from './join';
import { parse } from './parse';
import { substitute } from './substitute';
import { func } from './func';

/**
 * `SET path = value`
 *
 * @param path `parse`
 * @param value `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.SET
 */
export function set(path: string | ExpressionSpec, value: unknown): ExpressionSpec {
    return create(Clause.Set, equal(path, value));
}

/**
 * `a + b`
 *
 * @param a `parse`
 * @param b `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.SET.IncrementAndDecrement
 */
export function plus(a: string | ExpressionSpec, b: number): ExpressionSpec {
    return join(' + ', [
        parse(a),
        substitute(b),
    ]);
}

/**
 * `a - b`
 *
 * @param a `parse`
 * @param b `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.SET.IncrementAndDecrement
 */
export function minus(a: string | ExpressionSpec, b: number): ExpressionSpec {
    return join(' - ', [
        parse(a),
        substitute(b),
    ]);
}

/**
 * `list_append (a, b)`
 *
 * @param a `parse`
 * @param b `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.SET.UpdatingListElements
 */
export function listAppend(a: string | ExpressionSpec, b: unknown): ExpressionSpec {
    return func('list_append', [
        parse(a),
        substitute(b),
    ]);
}

/**
 * `if_not_exists (path, value)`
 *
 * @param path `parse`
 * @param value `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.SET.PreventingAttributeOverwrites
 */
export function ifNotExists(path: string | ExpressionSpec, value: unknown): ExpressionSpec {
    return func('if_not_exists', [
        parse(path),
        substitute(value),
    ]);
}
