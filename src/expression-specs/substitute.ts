import { ExpressionSpec, isExpressionSpec, create } from '../expression-spec';
import { Values } from '../placeholders';

/**
 * `:value`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.ExpressionAttributeValues.html
 */
export function substitute(value: unknown): ExpressionSpec {
    if (isExpressionSpec(value)) {
        return value;
    }

    return create(null, (_: unknown, values: Values): string => values.add(value));
}
