import { Check } from '@typemon/check';

import { ExpressionSpec, create, getClause, express } from '../expression-spec';
import { Names, Values } from '../placeholders';
import { Clause } from '../clause';

type Expressions = [
    null | string,
    null | string,
    null | string,
    null | string,
];

/**
 * @example
 * update(
 *     set(path, value),
 *     remove(path),
 *     add(path, value),
 *     del(path, value),
 *     [
 *         set(path, value),
 *         remove(path),
 *         add(path, value),
 *         del(path, value),
 *     ],
 * )
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html
 */
export function update(...expressionSpecs: ReadonlyArray<ExpressionSpec | ReadonlyArray<ExpressionSpec>>): ExpressionSpec {
    const flattenedExpressionSpecs: ReadonlyArray<ExpressionSpec> = expressionSpecs.flat();

    if (Check.equal(flattenedExpressionSpecs.length, 0)) {
        throw new Error('Provide at least one update expression specification.');
    }

    return create(null, (names: Names, values: Values): string => flattenedExpressionSpecs
        .reduce((expressions: Expressions, expressionSpec: ExpressionSpec): Expressions => {
            const clause: null | Clause = getClause(expressionSpec);

            if (Check.isNull(clause)) {
                throw new Error('Invalid update expression specification.');
            }

            const expression: string = express(expressionSpec, names, values);
            const previousExpression: null | string = expressions[clause];

            if (Check.isNull(previousExpression)) {
                expressions[clause] = expression;
            }
            else {
                expressions[clause] = `${previousExpression}, ${expression}`;
            }

            return expressions;
        }, [null, null, null, null])
        .map((expression: null | string, clause: Clause): null | string => Check.isNull(expression)
            ? null
            : `${Clause.stringify(clause)} ${expression}`)
        .filter(Check.isNotNull)
        .join(' '));
}
