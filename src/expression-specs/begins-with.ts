import { ExpressionSpec } from '../expression-spec';
import { func } from './func';
import { parse } from './parse';
import { substitute } from './substitute';

/**
 * `begins_with (path, value)`
 *
 * @param path `parse`
 * @param value `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Functions
 */
export function beginsWith(path: string | ExpressionSpec, value: string): ExpressionSpec {
    return func('begins_with', [
        parse(path),
        substitute(value),
    ]);
}
