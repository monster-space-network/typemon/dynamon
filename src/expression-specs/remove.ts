import { Check } from '@typemon/check';

import { ExpressionSpec, create } from '../expression-spec';
import { Clause } from '../clause';
import { parse } from './parse';
import { join } from './join';

/**
 * `REMOVE path`
 *
 * @param paths `parse`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.REMOVE
 */
export function remove(...paths: ReadonlyArray<string | ExpressionSpec | ReadonlyArray<string | ExpressionSpec>>): ExpressionSpec {
    const flattenedPaths: ReadonlyArray<string | ExpressionSpec> = paths.flat();

    if (Check.equal(flattenedPaths.length, 0)) {
        throw new Error('Provide at least one paths.');
    }

    return create(Clause.Remove, join(', ', flattenedPaths.map(parse)));
}
