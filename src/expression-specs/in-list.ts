import { Check } from '@typemon/check';

import { ExpressionSpec } from '../expression-spec';
import { join } from './join';
import { parse } from './parse';
import { func } from './func';
import { substitute } from './substitute';

/**
 * `a IN (b, c, d ...)`
 *
 * @param a `parse`
 * @param values `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Comparators
 */
export function inList(a: string | ExpressionSpec, values: ReadonlyArray<unknown>): ExpressionSpec {
    if (Check.equal(values.length, 0)) {
        throw new Error('Provide at least one value.');
    }

    return join(' ', [
        parse(a),
        func('IN', values.map(substitute)),
    ]);
}
