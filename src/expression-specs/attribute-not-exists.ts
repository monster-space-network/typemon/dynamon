import { ExpressionSpec } from '../expression-spec';
import { func } from './func';
import { parse } from './parse';

/**
 * `attribute_not_exists (path)`
 *
 * @param path `parse`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Functions
 */
export function attributeNotExists(path: string | ExpressionSpec): ExpressionSpec {
    return func('attribute_not_exists', [
        parse(path),
    ]);
}
