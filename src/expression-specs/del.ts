import { ExpressionSpec, create } from '../expression-spec';
import { Clause } from '../clause';
import { parse } from './parse';
import { join } from './join';
import { substitute } from './substitute';

/**
 * `DELETE path value`
 *
 * @param path `parse`
 * @param value `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.UpdateExpressions.html#Expressions.UpdateExpressions.DELETE
 */
export function del(path: string | ExpressionSpec, value: unknown): ExpressionSpec {
    return create(Clause.Delete, join(' ', [
        parse(path),
        substitute(value),
    ]));
}
