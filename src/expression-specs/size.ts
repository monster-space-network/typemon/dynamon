import { ExpressionSpec } from '../expression-spec';
import { func } from './func';
import { parse } from './parse';

/**
 * `size (path)`
 *
 * @param path `parse`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Functions
 */
export function size(path: string | ExpressionSpec): ExpressionSpec {
    return func('size', [
        parse(path),
    ]);
}
