import { Check } from '@typemon/check';

import { ExpressionSpec } from '../expression-spec';
import { join } from './join';
import { parse } from './parse';

/**
 * `path, path, path ...`
 *
 * @param paths `parse`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.ProjectionExpressions.html
 */
export function project(...paths: ReadonlyArray<string | ExpressionSpec | ReadonlyArray<string | ExpressionSpec>>): ExpressionSpec {
    const flattenedPaths: ReadonlyArray<string | ExpressionSpec> = paths.flat();

    if (Check.equal(flattenedPaths.length, 0)) {
        throw new Error('Provide at least one path.');
    }

    return join(', ', flattenedPaths.map(parse));
}
