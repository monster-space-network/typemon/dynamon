import { ExpressionSpec } from '../expression-spec';
import { join } from './join';
import { parse } from './parse';
import { substitute } from './substitute';

/**
 * `a = b`
 *
 * @param a `parse`
 * @param b `substitute`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.Comparators
 */
export function equal(a: string | ExpressionSpec, b: unknown): ExpressionSpec {
    return join(' = ', [
        parse(a),
        substitute(b),
    ]);
}
