import { ExpressionSpec, create } from '../expression-spec';
import { join } from './join';

/**
 * `NOT expression`
 *
 * @see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Expressions.OperatorsAndFunctions.html#Expressions.OperatorsAndFunctions.LogicalEvaluations
 */
export function not(expressionSpec: ExpressionSpec): ExpressionSpec {
    return join(' ', [
        create(null, 'NOT'),
        expressionSpec,
    ]);
}
