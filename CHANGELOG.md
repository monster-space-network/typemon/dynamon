# [4.3.0](https://gitlab.com/monster-space-network/typemon/dynamon/compare/4.2.0...4.3.0) (2021-02-19)


### Bug Fixes

* **scan:** 누락된 값 마샬링 추가 ([4707163](https://gitlab.com/monster-space-network/typemon/dynamon/commit/470716396ecd8b98838f8dad61073b0652f12afc))
* 빈 페이지가 반환되지 않는 문제 해결 ([872dca3](https://gitlab.com/monster-space-network/typemon/dynamon/commit/872dca3f1617b45b7a60a05612cf2698ace1f0c5))


### Features

* 페이지 인덱스 추가 및 페이지네이션 방식 변경 ([beff86e](https://gitlab.com/monster-space-network/typemon/dynamon/commit/beff86eb0ac46903a49539533c7292d9aec2fddb))
* **paginator:** 인덱스 추가 ([1f25958](https://gitlab.com/monster-space-network/typemon/dynamon/commit/1f259582a28ae0c095be23af9bf2990c7e323aa4))
* **paginator:** 조건부 연결 기능 추가 ([cc0fcb9](https://gitlab.com/monster-space-network/typemon/dynamon/commit/cc0fcb957cf54af56db7474e7d6de274f6f432ab))



# [4.2.0](https://gitlab.com/monster-space-network/typemon/dynamon/compare/4.1.0...4.2.0) (2021-02-18)


### Bug Fixes

* 잘못된 빈 페이지 스킵 조건 수정 ([5b44202](https://gitlab.com/monster-space-network/typemon/dynamon/commit/5b44202d744a23bc3ace9436bcd68b3e15667da8))


### Features

* 페이지 아이템 유형 제네릭 지원 ([46327e4](https://gitlab.com/monster-space-network/typemon/dynamon/commit/46327e47e70db136b52cdff28feb6e687a81f18b))
* 페이지네이터 기능 확장 ([945f26f](https://gitlab.com/monster-space-network/typemon/dynamon/commit/945f26ffd5c2a71b4b4780db894b468f2dcd4eac))



# [4.1.0](https://gitlab.com/monster-space-network/typemon/dynamon/compare/4.0.0...4.1.0) (2021-02-13)


### Features

* 배치 작업 분할 기능 추가 ([9bf6396](https://gitlab.com/monster-space-network/typemon/dynamon/commit/9bf639698faeccb40f69f273eb7158fc817926a8))
* 빈 페이지 스킵 기능 추가 ([b14ae4c](https://gitlab.com/monster-space-network/typemon/dynamon/commit/b14ae4c327d8d7fc0016bb2af532ac7c20cf6cfd))



# [4.0.0](https://gitlab.com/monster-space-network/typemon/dynamon/compare/3.0.0...4.0.0) (2021-02-03)


### Features

* 빈 페이지 스킵 기능 삭제 ([70945f2](https://gitlab.com/monster-space-network/typemon/dynamon/commit/70945f25dd20ede871387511d57787a517cf4248))
* 전체 쿼리 및 스캔 작업의 제한 무시 기능 삭제 ([32016b4](https://gitlab.com/monster-space-network/typemon/dynamon/commit/32016b47892871db6da5f8d2560164173d80f209))
* AWS SDK v3 지원 ([2fb8e12](https://gitlab.com/monster-space-network/typemon/dynamon/commit/2fb8e122965c595c8451a2f4107db4b95c867e89))
* returnValuesOnConditionCheckFailure 옵션 지원 ([fb35303](https://gitlab.com/monster-space-network/typemon/dynamon/commit/fb353039d21dc3bb63b1fd9183a2353b9dbba03d))


### BREAKING CHANGES

* 쿼리 및 스캔 작업의 빈 페이지 스킵 기능이 삭제되었습니다.



# [3.0.0](https://gitlab.com/monster-space-network/typemon/dynamon/compare/3.0.0-rc.0...3.0.0) (2020-08-29)



# [3.0.0-rc.0](https://gitlab.com/monster-space-network/typemon/dynamon/compare/3.0.0-next.7...3.0.0-rc.0) (2020-08-21)



# [3.0.0-next.7](https://gitlab.com/monster-space-network/typemon/dynamon/compare/3.0.0-next.6...3.0.0-next.7) (2020-08-19)


### Features

* **dynamon:** 배치 작업 기능 추가 ([cd2d223](https://gitlab.com/monster-space-network/typemon/dynamon/commit/cd2d22339a8d895590ac712336a7df2c439db927)), closes [#3](https://gitlab.com/monster-space-network/typemon/dynamon/issues/3)



# [3.0.0-next.6](https://gitlab.com/monster-space-network/typemon/dynamon/compare/3.0.0-next.5...3.0.0-next.6) (2020-08-18)


### Bug Fixes

* **dynamon.get:** 잘못된 결과 처리 방식 수정 ([ad0e4f0](https://gitlab.com/monster-space-network/typemon/dynamon/commit/ad0e4f042f5d327f9a101327183de09e9ff3a5d5))


### Features

* **dynamon:** 아이템 유형 삭제 ([dc9ea3d](https://gitlab.com/monster-space-network/typemon/dynamon/commit/dc9ea3d9d4b3abacdb7db1d5272c74dd759737ae))
* **dynamon.page:** lastEvaluatedKey 유형 변경 ([3d49567](https://gitlab.com/monster-space-network/typemon/dynamon/commit/3d49567a11508c982f41dd5539be442016b7661e))
* **dynamon.transactGet:** 반환 유형 변경 ([64d58ea](https://gitlab.com/monster-space-network/typemon/dynamon/commit/64d58ea53658729811cea0473627d468c869fb73))



# [3.0.0-next.5](https://gitlab.com/monster-space-network/typemon/dynamon/compare/3.0.0-next.4...3.0.0-next.5) (2020-08-18)


### Bug Fixes

* **dynamon:** 잘못된 트랜잭션 업데이트 인터페이스 수정 ([92a10fc](https://gitlab.com/monster-space-network/typemon/dynamon/commit/92a10fc24395187d5d2e3ef4ea2eb1a4df725a39))
* **dynamon:** 잘못된 트랜잭션 조건 확인 인터페이스 수정 ([aaebb0a](https://gitlab.com/monster-space-network/typemon/dynamon/commit/aaebb0ab98c7ab4c0ed470f75f4e1f5d5fcbf7ad))



# [3.0.0-next.4](https://gitlab.com/monster-space-network/typemon/dynamon/compare/3.0.0-next.3...3.0.0-next.4) (2020-08-18)


### Features

* **expression-specs.parse:** 캐싱 적용 ([3574f07](https://gitlab.com/monster-space-network/typemon/dynamon/commit/3574f073d4afefaff4f9946a7106ac77dede23c9)), closes [#9](https://gitlab.com/monster-space-network/typemon/dynamon/issues/9)
* **path-parser:** 경로 처리 방식 변경 ([089acbc](https://gitlab.com/monster-space-network/typemon/dynamon/commit/089acbc85f6f32d63b016eb7da3b05640e44038b))



# [3.0.0-next.3](https://gitlab.com/monster-space-network/typemon/dynamon/compare/3.0.0-next.2...3.0.0-next.3) (2020-08-17)


### Features

* **dynamon:** 병렬 스캔 옵션 추가 ([e2e290a](https://gitlab.com/monster-space-network/typemon/dynamon/commit/e2e290aad1c6fc288e0f48689042d380fc576665)), closes [#8](https://gitlab.com/monster-space-network/typemon/dynamon/issues/8)
* 의존성 업데이트 ([1a51923](https://gitlab.com/monster-space-network/typemon/dynamon/commit/1a51923ca891eeaeeb58b89da706e21b0c6feb1f))



# [3.0.0-next.2](https://gitlab.com/monster-space-network/typemon/dynamon/compare/3.0.0-next.1...3.0.0-next.2) (2020-08-16)


### Features

* **dynamon:** 빈 페이지 건너뛰기 기능 추가 ([4d1d21b](https://gitlab.com/monster-space-network/typemon/dynamon/commit/4d1d21bba367d76a3e5eb947884b5e56c2a44a91))
* **dynamon:** 페이지네이터 추가 ([7ee1572](https://gitlab.com/monster-space-network/typemon/dynamon/commit/7ee15726d34edc857254944cfe125857a5c5f6ec)), closes [#7](https://gitlab.com/monster-space-network/typemon/dynamon/issues/7)



# [3.0.0-next.1](https://gitlab.com/monster-space-network/typemon/dynamon/compare/3.0.0-next.0...3.0.0-next.1) (2020-08-14)


### Features

* **dynamon.page:** count, scannedCount 추가 ([a59bb92](https://gitlab.com/monster-space-network/typemon/dynamon/commit/a59bb92d8073ddeb846eddab3b07cd3f8db0a583))



# [3.0.0-next.0](https://gitlab.com/monster-space-network/typemon/dynamon/compare/2.0.1...3.0.0-next.0) (2020-08-14)


### Features

* **dynamon:** documentClient 기본 값 추가 ([72c1b79](https://gitlab.com/monster-space-network/typemon/dynamon/commit/72c1b7954716ca08bfc24502714055cae54679ac))
* **dynamon:** 변환 기능 추가 ([af8945e](https://gitlab.com/monster-space-network/typemon/dynamon/commit/af8945e747ac7a8cb6aaa48187c2047a1454134a)), closes [#5](https://gitlab.com/monster-space-network/typemon/dynamon/issues/5)
* **dynamon:** 읽기 일관성 옵션 추가 ([60dc364](https://gitlab.com/monster-space-network/typemon/dynamon/commit/60dc364e00a75834472654fcf06a6901d2bc2a19)), closes [#6](https://gitlab.com/monster-space-network/typemon/dynamon/issues/6)
* **dynamon:** 클라이언트 값 이름 변경 ([8396945](https://gitlab.com/monster-space-network/typemon/dynamon/commit/83969456e56536fa0e18f6abff38047a4f106ad9))
* **dynamon:** 트랜잭션 기능 추가 ([0f00666](https://gitlab.com/monster-space-network/typemon/dynamon/commit/0f00666e4b32a7833dcceaed8058bcfbee9b7bc5)), closes [#4](https://gitlab.com/monster-space-network/typemon/dynamon/issues/4)
* dynamon 추가 ([198d9ce](https://gitlab.com/monster-space-network/typemon/dynamon/commit/198d9ceb5d0d7f086fc9b521aea6e638578cb7d0))



## [2.0.1](https://gitlab.com/monster-space-network/typemon/dynamon/compare/2.0.0...2.0.1) (2020-08-02)


### Bug Fixes

* **expression-spec:** 누락된 캐시 처리 추가 ([7efc8cc](https://gitlab.com/monster-space-network/typemon/dynamon/commit/7efc8cc596944e450af8c2af7dc0223002821347))



# [2.0.0](https://gitlab.com/monster-space-network/typemon/dynamon/compare/1.3.0...2.0.0) (2020-08-02)


### Features

* 범위 기능 위치 변경 ([1af6e33](https://gitlab.com/monster-space-network/typemon/dynamon/commit/1af6e3358572bda1303d1b82db9f9b32472c8678))
* **expression-spec-scope:** 절 처리 추가 ([e839d7d](https://gitlab.com/monster-space-network/typemon/dynamon/commit/e839d7dfd7f51f41084cfa1a8f671153ac631d1a))
* **expression-specs:** project 추가 ([c79ca6a](https://gitlab.com/monster-space-network/typemon/dynamon/commit/c79ca6ac5dd440c6760cf9744003b494f206b3f6))
* **expression-specs.remove:** 편의성 기능 추가 ([d1871da](https://gitlab.com/monster-space-network/typemon/dynamon/commit/d1871da7d342a90189128f7a42a51f8359c70e5a))
* expression-spec-scope 추가 ([6874d1e](https://gitlab.com/monster-space-network/typemon/dynamon/commit/6874d1e8ac31b430e49f466eceba7ce11e92ba2c))
* **expression-specs.set:** plus, minus, listAppend, ifNotExists 추가 ([e68e74b](https://gitlab.com/monster-space-network/typemon/dynamon/commit/e68e74b5d5ace89a64fb3b64da2a127af15379ed))
* 의존성 업데이트 ([bfbe718](https://gitlab.com/monster-space-network/typemon/dynamon/commit/bfbe71820223b893744b950fd18ab2699a7a64ac))
* 재작성 ([af8b1ee](https://gitlab.com/monster-space-network/typemon/dynamon/commit/af8b1eeb92adee8a174674414760491c55c22541))



# [1.3.0](https://gitlab.com/monster-space-network/typemon/dynamon/compare/1.2.0...1.3.0) (2020-04-14)


### Bug Fixes

* **expression-specs.not:** 잘못된 키워드 수정 ([0005711](https://gitlab.com/monster-space-network/typemon/dynamon/commit/000571115382664523bc94923a092b0aef162420))


### Features

* types 수출 ([7c1c87f](https://gitlab.com/monster-space-network/typemon/dynamon/commit/7c1c87f482e034214b827e344ea034600bca614e))
* **expression-specs.and:** 괄호 기능 추가 ([7f4deba](https://gitlab.com/monster-space-network/typemon/dynamon/commit/7f4debaef175abaecc2830054c74054971a93646))
* **expression-specs.or:** 괄호 기능 추가 ([6033780](https://gitlab.com/monster-space-network/typemon/dynamon/commit/60337802273552bafd8b805a4b9dc3c1b45662dc))



# [1.2.0](https://gitlab.com/monster-space-network/typemon/dynamon/compare/1.1.0...1.2.0) (2020-04-10)


### Features

* **expression-specs:** project 추가 ([e1a3e2b](https://gitlab.com/monster-space-network/typemon/dynamon/commit/e1a3e2b63e3657cc4ed6161531b427d611120ef5))



# [1.1.0](https://gitlab.com/monster-space-network/typemon/dynamon/compare/1.0.0...1.1.0) (2020-03-30)


### Features

* **utils.parse:** 이스케이프, 인덱스 지원 기능 및 유효성 검증 추가 ([ba46dd2](https://gitlab.com/monster-space-network/typemon/dynamon/commit/ba46dd2fea6cb5c300f1a2602258cd3f764eaf62))
